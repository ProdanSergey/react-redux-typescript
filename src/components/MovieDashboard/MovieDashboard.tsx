import React, { Fragment }  from 'react';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from "redux-thunk";

import { getMovieByID } from '../../store/Movies/actions';
 
import { AppState } from '../../store';
import * as Types from './types';

import BaseSpinner from '../UI/Spinner/Spinner';
import MovieCard from '../MovieCard/MovieCard';

import { makeIsMovieFetching, makeGetMovieByID } from '../../store/Movies/selectors';

const mapStateToProps = (state: AppState, props: Types.Props) => {
    const movieID = props.match.params.movieID;

    const isMovieFetchingSelector = makeIsMovieFetching(movieID);
    const getMovieSelector = makeGetMovieByID(movieID);

    return {
        movie: getMovieSelector(state),
        isLoading: isMovieFetchingSelector(state),
    };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, void, AnyAction>, props: Types.Props) => {
    const movieID = props.match.params.movieID;
    
    return {
        getMovieByID: () => dispatch(getMovieByID(movieID)),
    };
};

export class MovieDashboard extends React.Component<Types.Props> {
    
    componentDidMount() {
        this.props.getMovieByID();
    }

    componentDidUpdate(prevProps: Types.Props) {
        const { match: { params } } = this.props;
        const prevID: string = prevProps.match.params.movieID;

        if (params.movieID !== prevID) {
            this.props.getMovieByID();
        }
    }

    render() {
        const { movie, isLoading } = this.props;

        return (
            <Fragment>
                <MovieCard movie={movie}/>
                <BaseSpinner active={isLoading}/>
            </Fragment>
        )
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(MovieDashboard);