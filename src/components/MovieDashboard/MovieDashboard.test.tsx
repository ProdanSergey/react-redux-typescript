import React from 'react';

import { configure, shallow, ShallowWrapper } from 'enzyme';
import { expect } from 'chai';
import sinon, { SinonSpy } from 'sinon';
import Adapter from 'enzyme-adapter-react-16';

import { MovieDashboard } from './MovieDashboard';
import { MovieCard } from '../MovieCard/MovieCard';

import { responseMock } from './mock';
import BaseSpinner from '../UI/Spinner/Spinner';

configure({ adapter: new Adapter() });

describe('<MovieDashboard /> component', () => {
    let spy: SinonSpy, props: any, wrapper: ShallowWrapper;
    
    beforeEach(() => {
        spy = sinon.spy();

        props = {
            movie: responseMock,
            isLoading: false,
            getMovieByID: spy,
            match: {
                params: {movieID: responseMock.imdbID}
            },
        };

        wrapper = shallow(<MovieDashboard {...props} />);
    });

    describe('Component render', () => {
        it('Should render movie card with provided movie object', () => {
            expect(wrapper.find(MovieCard)).to.have.lengthOf(1);
            expect(wrapper.find(MovieCard).prop('movie')).to.be.deep.equal(responseMock);
        });

        it('Should render spinner component', () => {
            expect(wrapper.find(BaseSpinner)).to.have.lengthOf(1);
        });

        it('Should show spinner in case movie is in loading state', () => {
            wrapper.setProps({isLoading: true});

            expect(wrapper.find(BaseSpinner).prop('active')).to.be.true;
        });
    });
    describe('Component methods', () => {
        it('Should fetch movie', () => {
            expect(spy.calledOnce).to.be.true;
        });
    });
});