import { RouteComponentProps } from "react-router-dom";

import { Movie } from '../MovieCard/types';

type RouterTParams = { movieID: string };

export interface StateProps extends RouteComponentProps<RouterTParams> {
    movie: Movie;
    isLoading: boolean;
};

export interface DispatchProps {
    getMovieByID: () => Promise<any> | Movie;
};

export type Props = StateProps & DispatchProps;