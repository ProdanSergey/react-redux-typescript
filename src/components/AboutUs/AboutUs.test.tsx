import React from 'react';

import { configure, shallow, ShallowWrapper } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';

import AboutUs from './AboutUs';

configure({ adapter: new Adapter() });

describe('<AboutUs /> component', () => {
  let wrapper: ShallowWrapper;

  beforeEach(() => {
    wrapper = shallow(<AboutUs />);
  });

  describe('Component render', () => {
    it('Should render title inside AboutUs component', () => {
        expect(wrapper.find('h1')).to.have.lengthOf(1);
    });
  });
});