import React  from 'react';
import Image from 'react-bootstrap/Image';

import styles from './AboutUs.module.scss';

class AboutUs extends React.Component<{}> {
    render() {
        return (
            <div className={styles.container}>
                <h1 className={styles.title}>Wellcome here traveller!</h1>
                <Image src="simpson.gif"></Image>
            </div>
        );
    };
};

export default AboutUs;