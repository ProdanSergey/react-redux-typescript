import React from 'react';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from "redux-thunk";

import { getMovieByID } from '../../store/Movies/actions';

import Card from 'react-bootstrap/Card';
import BaseSpinner from '../UI/Spinner/Spinner';
import FavIcon from '../UI/FavIcon/FavIcon';

import { AppState } from '../../store';
import { ownProps, Props } from './types';

import { makeIsMovieFetching, makeGetMovieByID } from '../../store/Movies/selectors';

import { IMG_PATH_REGXP, IMG_PLACEHOLDER_PATH } from '../../constants/images';

import styles from './FavouriteCard.module.scss';
import { Movie } from '../MovieCard/types';
import { Link } from 'react-router-dom';

const mapStateToProps = (state: AppState, props: ownProps) => {
    const isMovieFetchingSelector = makeIsMovieFetching(props.movieID);
    const getMovieSelector = makeGetMovieByID(props.movieID);

    return {
        movie: getMovieSelector(state),
        isLoading: isMovieFetchingSelector(state),
    };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, void, AnyAction>) => {
    return {
        getMovieByID: (movieID: string) => dispatch(getMovieByID(movieID)), 
    };
};

export class FavouriteCard extends React.Component<Props> {
    componentDidMount() {
        this.props.getMovieByID(this.props.movieID);
    }

    getPosterSrc(src: string = '') {
        return src.match(IMG_PATH_REGXP) ? src : IMG_PLACEHOLDER_PATH;
    }

    renderTitle(movie: Movie) {
        return (
            <Card.Title className={styles.title}>
                <Card.Link as={Link} to={`/${movie.imdbID}`}>{movie.Title}</Card.Link>
            </Card.Title>
        )
    }

    renderCard() {
        const {movie, isLoading } = this.props;
        
        return (
            <Card className={styles.container}>
                <Card.Img src={this.getPosterSrc(movie.Poster)} alt={movie.Title} className={styles.background} />
                <Card.ImgOverlay className={styles.description}>{this.renderTitle(movie)}</Card.ImgOverlay>
                <FavIcon movieID={this.props.movieID} />
                <BaseSpinner active={isLoading} />
            </Card>
        );
    }

    render() {
        return this.renderCard();
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(FavouriteCard);