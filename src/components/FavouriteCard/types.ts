import { Movie } from '../MovieCard/types';

export interface ownProps {
    movieID: string;
};

export interface StateProps {
    movie: Movie;
    isLoading: boolean;
};

export interface DispatchProps {
    getMovieByID: (movieID: string) => Promise<any> | Movie;
};

export type Props = ownProps & StateProps & DispatchProps;