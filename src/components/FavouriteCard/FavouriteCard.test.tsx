import React from 'react';

import { configure, shallow, ShallowWrapper } from 'enzyme';
import { expect } from 'chai';
import sinon, { SinonSpy } from 'sinon';
import Adapter from 'enzyme-adapter-react-16';

import { responseMock } from '../../components/MovieDashboard/mock';

import { FavouriteCard } from './FavouriteCard';
import FavIcon from '../UI/FavIcon/FavIcon';
import Card from 'react-bootstrap/Card';
import BaseSpinner from '../UI/Spinner/Spinner';

configure({ adapter: new Adapter() });

describe('<FavouriteCard /> component', () => {
    let props: any, spy: SinonSpy, wrapper: ShallowWrapper;

    beforeEach(() => {
        spy = sinon.spy();

        props = {
            movie: responseMock,
            movieID: responseMock.imdbID,
            isLoading: false,
            getMovieByID: spy,
        };

        wrapper =  shallow(<FavouriteCard {...props} />);
    });

    describe('Component render', () => {
        it('Should render favourite movie card', () => {

            expect(wrapper.find(Card)).to.have.lengthOf(1);
        });
        it('Should render movie poster with provided src link', () => {

            expect(wrapper.find(Card.Img).prop('src')).to.equal(responseMock.Poster);
        });
        it('Should render poster placeholder in case provided src is incorect', () => {
            wrapper.setProps({movie: {
                ...responseMock,
                Poster: 'N/A',
            }});

            expect(wrapper.find(Card.Img).prop('src')).to.equal('img-placeholder.jpg');
        });
        it('Should render fav icon component', () => {

            expect(wrapper.find(FavIcon)).to.be.lengthOf(1);
            expect(wrapper.find(FavIcon).prop('movieID')).to.equal(props.movieID);
        });
        it('Should show spinner in case movie state isLoading', () => {
            wrapper.setProps({isLoading: true});

            expect(wrapper.find(BaseSpinner)).to.be.lengthOf(1);
            expect(wrapper.find(BaseSpinner).prop('active')).to.be.true;
        });
    });
    describe('Component methods', () => {
        it('Should fetch movie on component mount', () => {
            expect(spy.calledOnceWith(props.movieID)).to.be.true;
        });
    });
});