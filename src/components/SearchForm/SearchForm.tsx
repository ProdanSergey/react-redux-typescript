import React  from 'react';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from "redux-thunk";

import Form from 'react-bootstrap/Form';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faSearch } from '@fortawesome/free-solid-svg-icons';

import { push } from 'connected-react-router';
import { searchMovieByName } from '../../store/SearchMovieForm/actions';

import { AppState } from '../../store';
import { Props, TypeheadOption, SelectedOptions } from './types';

import styles from './SearchForm.module.scss';

const mapStateToProps = (state: AppState) => {
    return {
        isLoading: state.searchMovieForm.isLoading,
        options: state.searchMovieForm.result,
    };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, void, AnyAction>) => {
    return {
        searchMovieByName: (query: string) => dispatch(searchMovieByName(query)),
        push: (movieID: string) => dispatch(push(movieID)),
    };
};

export class SearchForm extends React.Component<Props> {

    handleSearch = (query: string): void => this.props.searchMovieByName(query);
    handleChange = (selected: SelectedOptions): void => {
        if (!selected.length) return;
        
        const item: TypeheadOption = selected[0] || {}; 
        const id: string = item.imdbID || '';

        this.props.push(id);
    };
     
    renderOption(option: TypeheadOption) {
        return option.Title || '';
    }

    render() {
        const { isLoading, options } = this.props;

        return (
            <Form name="search-movie" className={styles.container}>
                <label className={styles.label}>
                    <FontAwesomeIcon icon={faSearch}/>
                    <span className={styles.label__text}>Let's find something!</span>
                </label>
                <AsyncTypeahead
                    id="search-movie"
                    placeholder="Search a movie..."
                    labelKey={this.renderOption}
                    options={options}
                    onSearch={this.handleSearch}
                    onChange={this.handleChange}
                    isLoading={isLoading}
                    clearButton 
                />
            </Form>
        );
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SearchForm);