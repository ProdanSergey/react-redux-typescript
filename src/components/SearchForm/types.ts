export interface StateProps {
    isLoading: boolean;
    options: Array<any>;
};

export interface DispatchProps {
    searchMovieByName: (query: string) => void;
    push: (movieID: string) => void;
};

export interface TypeheadOption {
    Title?: string;
    Year?: string;
    imdbID?: string;
    Type?: string;
    Poster?: string;
};

export interface SelectedOptions {
    [index: number]: object;
    length: number;
};

export type Props = StateProps & DispatchProps;