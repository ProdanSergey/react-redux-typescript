import React from 'react';

import { configure, ShallowWrapper, shallow } from 'enzyme';
import { expect } from 'chai';
import sinon, { SinonSpy } from 'sinon';

import Adapter from 'enzyme-adapter-react-16';

import { SearchForm } from './SearchForm';

import { AsyncTypeahead } from 'react-bootstrap-typeahead';

import { responseMock } from './mock';

configure({ adapter: new Adapter() });

describe('SearchForm component', () => {
    let props: any, wrapper: ShallowWrapper, spy: SinonSpy;

    beforeEach(() => {
        spy = sinon.spy();

        props = {
            options: responseMock.Search,
            isLoading: false,
            searchMovieByName: spy,
        }

        wrapper = shallow(<SearchForm {...props} />);
    });

    describe('Component render', () => {
        it('Should render Async Typehead component', () => {
            const typehead = wrapper.find(SearchForm).find(AsyncTypeahead);
            
            expect(typehead).not.to.be.null;
        });
    });
});