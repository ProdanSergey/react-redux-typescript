import React from 'react';

import { configure, shallow, ShallowWrapper } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';

import configureStore, { MockStore } from 'redux-mock-store';
import { rootState } from '../../store';

import { responseMock } from '../../components/MovieDashboard/mock';

import { MovieCard } from './MovieCard';
import Alert from 'react-bootstrap/Alert';
import ProgressBar from 'react-bootstrap/ProgressBar';
import FavIcon from '../UI/FavIcon/FavIcon';

import * as Utils from './utils';

configure({ adapter: new Adapter() });

const mockStore = configureStore();

describe('<MovieCard /> component', () => {
    let store: MockStore = mockStore(rootState);
    let props = { movie: responseMock };

    let wrapper: ShallowWrapper;

    beforeEach(() => {
        wrapper = shallow(<MovieCard {...props} />);
    });

    describe('Movie Card render procces', () => {
        it('Should render movie card', () => {

            expect(wrapper.find('#movie-card')).to.have.lengthOf(1);
        });

        it('Should render error', () => {
            wrapper.setProps({
                movie: {
                    Response: 'False',
                    Error: 'Could not find movie ID',
                }
            });

            expect(wrapper.find(Alert)).to.have.lengthOf(1);
            expect(wrapper.find(Alert).prop('variant')).to.equal('warning');
            expect(wrapper.find(Alert).text()).to.equal('Could not find movie ID');
        });
    });

    describe('Movie Poster render procces', () => {
        it('Should render movie poster', () => {

            expect(wrapper.find('#movie-poster')).to.have.lengthOf(1);
        });
        // it('Should render FavIcon component at movie poster', () => {

        //     expect(wrapper.find('#movie-rating').find(FavIcon)).to.have.lengthOf(1);
        //     expect(wrapper.find('#movie-rating').find(FavIcon).prop('movieID')).to.equal(props.movie.imdbID);
        // });
    });

    describe('Movie Rating render procces', () => {
        it('Should render movie rating', () => {
            expect(wrapper.find('#movie-rating')).to.have.lengthOf(1);
        });

        it('Should render ProgressBar with movie rating inside Movie Rating', () => {
            const rating = Utils.getRationForMovie(props.movie);

            expect(wrapper.find('#movie-rating').find(ProgressBar)).to.have.lengthOf(1);
            expect(wrapper.find('#movie-rating').find(ProgressBar).prop('now')).to.be.equal(rating.ratio);
            expect(wrapper.find('#movie-rating').find(ProgressBar).prop('label')).to.be.equal(rating.text);
        });
    
        it('Should not render movie rating in case rating is not provided', () => {
            wrapper.setProps({
                movie: {
                    ...responseMock,
                    imdbRating: undefined,
                }
            });

            expect(wrapper.find('#movie-rating')).to.be.empty;
        });
    })
});