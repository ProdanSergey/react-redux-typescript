import React, { Fragment } from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import ProgressBar from 'react-bootstrap/ProgressBar';
import Badge from 'react-bootstrap/Badge';
import FavIcon from '../UI/FavIcon/FavIcon';
import Alert from 'react-bootstrap/Alert';

import * as Types from './types';

import * as Utils from './utils';

import { IMG_PATH_REGXP, IMG_PLACEHOLDER_PATH } from '../../constants/images';
import { URL_REGEXP } from '../../constants/url';

import styles from './MovieCard.module.scss';

export class MovieCard extends React.Component<Types.Props> {

    getPosterSrc(src: string = '') {
        return src.match(IMG_PATH_REGXP) ? src : IMG_PLACEHOLDER_PATH;
    }

    renderWebsiteUrl(url: string = '') {
        if (url.match(URL_REGEXP)) {
            return <Card.Link href={url}>{url}</Card.Link>;
        } else {
            return <span>Not available...</span>
        }
    }

    renderPlot(plot: string = '') {
        return <div className={styles.plot}>{plot}</div>
    }

    renderCredit(credit: Types.CreditForMovie, index: number) {
        return (
            <li className={styles.credit} key={index}>
                <div className={styles.credit__title}>{credit.title}</div>
                <div className={styles.credit__text}>{credit.text}</div>
            </li>
        );
    }

    renderCredits(credits: Array<Types.CreditForMovie>, headline: string) {
        return (
            <Fragment>
                <h3 className={styles.headline}>
                    {headline}
                </h3>
                <ul className={styles.credits}>
                    {credits.map((credit: Types.CreditForMovie, index: number) => this.renderCredit(credit, index))}
                </ul>
            </Fragment>
        );
    }

    renderError(error: string = '') {
        return <Alert variant="warning">{error}</Alert>
    }

    renderRating(movie: Types.Movie) {
        const entity: Types.MovieRatio = Utils.getRationForMovie(movie);
        
        return movie.imdbRating && (
            <div id="movie-rating" className={styles.rating}>
                <ProgressBar now={entity.ratio} label={entity.text}/>
                <span className={styles.rating__description}>Due to IMDB rating</span>
            </div>
        )
    }

    renderPoster(movie: Types.Movie) {
        return (
            <Card id="movie-poster">
                <Card.Img src={this.getPosterSrc(movie.Poster)} alt={movie.Title}/>
                <FavIcon movieID={movie.imdbID}/>
                <Badge className={styles.votes} variant="warning">{movie.imdbVotes}</Badge>
            </Card>
        );
    }

    renderMovie(movie: Types.Movie) {
        const credits: Array<Types.CreditForMovie> = Utils.getCreditsForMovie(movie);
        const addInfo: Array<Types.CreditForMovie> = Utils.getAdditionalInfo(movie);

        return (
            <Card id="movie-info">
                <Card.Header>
                    <h2 className={styles.title}>{movie.Title}</h2>
                    <span className={styles.year}>{movie.Year}</span>
                    <span className={styles.genre}>{movie.Genre}</span>
                </Card.Header>
                <Card.Body>
                    {this.renderPlot(movie.Plot)}
                    {this.renderCredits(credits, 'Credits')}
                    {this.renderCredits(addInfo, 'Additional Info')}
                </Card.Body>
                <Card.Footer>
                    Visit: {this.renderWebsiteUrl(movie.Website)}
                </Card.Footer>
            </Card>
        );
    }

    renderCard(movie: Types.Movie) {
        return (
            <Row id="movie-card" as="article" className={styles.card}>
                <Col>
                    {this.renderPoster(movie)}
                    {this.renderRating(movie)}
                </Col>
                <Col lg="9" className={styles.description}>
                    {this.renderMovie(movie)}
                </Col>
            </Row>
        );
    }

    render() {
        const { movie } = this.props;
        const shouldRenderError = !Utils.parseResponseProperty(movie.Response);

        if (shouldRenderError) {
            return this.renderError(movie.Error);
        }
        else {
            return this.renderCard(movie);
        }
    }
};

export default MovieCard;