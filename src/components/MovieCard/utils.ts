import * as Types from './types';

export const getCreditsForMovie = (movie: Types.Movie): Array<Types.CreditForMovie> => {
    return [
        {title: 'Director', text: movie.Director || ''},
        {title: 'Writer', text: movie.Writer || ''},
        {title: 'Actors', text: movie.Actors || ''},
    ];
};

export const getAdditionalInfo = (movie: Types.Movie): Array<Types.CreditForMovie> => {
    return [
        {title: 'Rated', text: movie.Rated || ''},
        {title: 'Released', text: movie.Released || ''},
        {title: 'Runtime', text: movie.Runtime || ''},
        {title: 'Language', text: movie.Language || ''},
        {title: 'Country', text: movie.Country || ''},
        {title: 'Awards', text: movie.Awards || ''},
        {title: 'Metascore', text: movie.Metascore || ''},
        {title: 'imdbRating', text: movie.imdbRating || ''},
        {title: 'imdbVotes', text: movie.imdbVotes || ''},
        {title: 'DVD', text: movie.DVD || ''},
        {title: 'BoxOffice', text: movie.BoxOffice || ''},
        {title: 'Production', text: movie.Production || ''},
    ];
};

export const getRationForMovie = (movie: Types.Movie): Types.MovieRatio => {
    const ratio = parseFloat(movie.imdbRating || '0') * 10;
    return {
        ratio,
        text: movie.imdbRating || '0',
    }
};

export const parseResponseProperty = (response: string = ''): boolean => {
    const _response = response.toLowerCase();

    switch (_response) {
        case 'true':
            return true;
        case 'false':
        default:
            return false;
    }
}