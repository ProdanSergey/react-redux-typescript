import React from 'react';

const NoMatch: React.FC = () => {
    return (
        <span>No Match</span>
    );
}

export default NoMatch