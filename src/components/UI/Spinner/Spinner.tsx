import React from 'react';

import { Props } from './types';

import Spinner from 'react-bootstrap/Spinner';

import styles from './Spinner.module.scss';

class BaseSpinner extends React.Component<Props> {
    static defaultProps = {
        active: false,
    };

    render() {
        return this.props.active && (
            <div className={styles.container}>
                <Spinner animation="grow" variant="primary"/>
            </div>
        );
    }
}

export default BaseSpinner;