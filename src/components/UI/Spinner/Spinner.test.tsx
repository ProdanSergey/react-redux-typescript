import React from 'react';

import { configure, shallow, ShallowWrapper } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';

import BaseSpinner from './Spinner';

import Spinner from 'react-bootstrap/Spinner';

configure({ adapter: new Adapter() });

describe('<BaseSpinner /> component', () => {
  let wrapper: ShallowWrapper;

  beforeEach(() => {
    wrapper = shallow(<BaseSpinner />);
  });

  describe('Component render', () => {
    
    it('Should not render bootstrap Spinner component', () => {
        expect(wrapper.find(Spinner)).to.have.lengthOf(0);
    });

    it('Should render bootstrap Spinner component in case active prop', () => {
        wrapper.setProps({active: true});
        
        expect(wrapper.find(Spinner)).to.have.lengthOf(1);
    });
  });
});