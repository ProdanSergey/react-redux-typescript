export interface ownProps {
    movieID?: string;
};

export interface stateProps {
    isFavourite?: boolean,
    handleFavourite?: () => any,
};

export type Props = ownProps & stateProps;