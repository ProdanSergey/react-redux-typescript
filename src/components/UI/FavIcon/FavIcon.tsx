import React from 'react';
import { connect } from 'react-redux';

import { AppState } from '../../../store';
import { ownProps, Props } from './types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Button from 'react-bootstrap/Button';

import { markMovieAsFavourite, unmarkMovieAsFavourite } from '../../../store/UserPref/actions';
import { makeIsMovieFavourite } from '../../../store/UserPref/selectors';

import { faStar as SolidStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as EmptyStar } from '@fortawesome/free-regular-svg-icons';

import styles from './FavIcon.module.scss';

const mapStateToProps = (state: AppState, props: ownProps) => {
    const isMovieFavourite = makeIsMovieFavourite(props.movieID);

    return {
        isFavourite: isMovieFavourite(state),
    };
}

const mergeProps = (stateProps: any, {dispatch} : any, ownProps: ownProps) => {
    const { movieID } = ownProps;
    
    const handleFavourite: () => void = stateProps.isFavourite 
        ? () => dispatch(unmarkMovieAsFavourite(movieID))
        : () => dispatch(markMovieAsFavourite(movieID))

    return {
        ...stateProps,
        movieID,
        handleFavourite,
    }
}

export class FavIcon extends React.Component<Props> {
    static defaultProps = {
        isFavourite: false,
        handleFavourite: () => void(0),
    };

    renderIcon() {
        const { isFavourite } = this.props;

        return isFavourite
            ? <FontAwesomeIcon icon={SolidStar}/>
            : <FontAwesomeIcon icon={EmptyStar}/>
    }

    render() {
        return (
            <Button 
                variant="link"
                onClick={this.props.handleFavourite}
                className={styles.trigger}
            >
                {this.renderIcon()}
            </Button>
        );
    }
}

export default connect(
    mapStateToProps,
    null,
    mergeProps,
)(FavIcon);