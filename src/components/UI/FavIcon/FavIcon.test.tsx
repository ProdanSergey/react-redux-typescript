import React from 'react';

import { configure, shallow, ShallowWrapper } from 'enzyme';
import { expect } from 'chai';
import sinon, { SinonSpy } from 'sinon';
import Adapter from 'enzyme-adapter-react-16';

import { FavIcon } from './FavIcon';

import { faStar as SolidStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as EmptyStar } from '@fortawesome/free-regular-svg-icons';

import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

configure({ adapter: new Adapter() });

describe('<FavIcon /> component', () => {
    let spy: SinonSpy, props: any, wrapper: ShallowWrapper;

    beforeEach(() => {
        spy = sinon.spy();

        props = {
            isFavourite: false,
            handleFavourite: spy,
        };

        wrapper = shallow(<FavIcon {...props} />);
    });

    describe('Component render', () => {
        it('Should render button that contain icon component', () => {

            expect(wrapper.find(Button)).to.have.lengthOf(1);
            expect(wrapper.find(Button).find(FontAwesomeIcon)).to.have.lengthOf(1);
        });

        it('Should render empty star as icon inside button in case movie is not favourite' , () => {
            expect(wrapper.find(Button).find(FontAwesomeIcon).prop('icon')).to.be.equal(EmptyStar);
        });

        it('Should render solid star as icon inside button in case movie is favourite' , () => {
            wrapper.setProps({isFavourite: true});

            expect(wrapper.find(Button).find(FontAwesomeIcon).prop('icon')).to.be.equal(SolidStar);
        })
    });
    describe('Component methods', () => {
        it('Should invoke handleFavourite method if user click on button', () => {
            wrapper.find(Button).simulate('click');

            expect(spy.called).to.be.true;
        });
    });
});