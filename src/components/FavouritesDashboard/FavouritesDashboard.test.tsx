import React from 'react';

import { configure, shallow, ShallowWrapper } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';

import { FavouritesDashboard } from './FavouritesDashboard';
import ConectedFavouriteCard, { FavouriteCard } from '../FavouriteCard/FavouriteCard';

import CardDeck from 'react-bootstrap/CardDeck';

configure({ adapter: new Adapter() });

describe('<FavouritesDashboard /> component', () => {
    const props = {
        favourites: ['tt123123', 'tt2312424', 'tt2321323'],
    };

    let wrapper: ShallowWrapper;

    beforeEach(() => {
        wrapper = shallow(<FavouritesDashboard {...props} />);
    });

    describe('Component render', () => {
        it('Should render deck of favourites card', () => {

            expect(wrapper.find(CardDeck)).to.have.lengthOf(1);
        });
        it('Should render favourites cards as a children of rendered card dack', () => {
            
            expect(wrapper.children()).to.have.lengthOf(3);
            wrapper.children().map(child => 
                expect(child.matchesElement(<ConectedFavouriteCard/>)).to.be.true
            );
        });
    });
});