import React  from 'react';
import { connect } from 'react-redux';

import CardDeck from 'react-bootstrap/CardDeck';

import FavouriteCard from '../FavouriteCard/FavouriteCard';

import { AppState } from '../../store';
import { Props } from './types';

const mapStateToProps = (state: AppState) => {
    return {
        favourites: state.userPref.favourites,
    };
};

export class FavouritesDashboard extends React.Component<Props> {
    render() {
        const { favourites } = this.props;

        return (
            <CardDeck>
                {favourites.map((movieID: string) => <FavouriteCard key={movieID} movieID={movieID} />)}
            </CardDeck>
        );
    };
};

export default connect(
    mapStateToProps,
)(FavouritesDashboard);