export interface Props {
    favourites: Array<string | never>;
}