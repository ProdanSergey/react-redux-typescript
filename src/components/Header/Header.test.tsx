import React from 'react';

import { configure, shallow, ShallowWrapper } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';

import Header from './Header';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import { Link } from 'react-router-dom';

configure({ adapter: new Adapter() });

describe('<Header /> component', () => {
  let wrapper: ShallowWrapper;

  beforeEach(() => {
    wrapper = shallow(<Header />);
  });

  describe('Component render', () => {
    it('Should render Navbar component', () => {
      expect(wrapper.find(Navbar)).to.have.lengthOf(1);
    });
    it('Should render Navbar Brand component', () => {
      expect(wrapper.find(Navbar).find(Navbar.Brand)).to.have.lengthOf(1);
    });
    it('Navbar Brand component should be an React-Router-Link component', () => {
      expect(wrapper.find(Navbar).find(Navbar.Brand).prop('as')).to.equal(Link);
    });
    it('Navbar Brand component should route to home page', () => {
      expect(wrapper.find(Navbar).find(Navbar.Brand).prop('to')).to.equal('/');
    });
    it('Should render Nav component', () => {
      expect(wrapper.find(Navbar).find(Nav)).to.have.lengthOf(1);
    });
    it('Nav component should render atleast one Nav.Link', () => {
      expect(wrapper.find(Navbar).find(Nav).children().length).to.be.least(1);
    });
    it('Nav component should contain Nav.Link component that route to /favourites', () => {
      expect(wrapper.find(Navbar).find(Nav).children().someWhere(child => child.prop('to') === '/favourites')).to.be.true;
    });
  });
});