import React  from 'react';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import { Link } from 'react-router-dom';

class Header extends React.Component<{}, {}> {
    render() {
        return (
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand as={Link} to="/">GetMovieDB!</Navbar.Brand>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link as={Link} to="/favourites">Favourites</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    };
};

export default Header;