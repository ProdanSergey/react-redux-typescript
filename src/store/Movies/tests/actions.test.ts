import { expect } from 'chai';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import configureMockStore, { MockStore, MockStoreCreator } from 'redux-mock-store';

import thunk from 'redux-thunk';

import * as Actions from '../actions';
import * as Types from '../types';

import { rootState } from '../../../store';

import * as API from '../../../constants/api';
import { responseMock } from '../../../components/MovieDashboard/mock';

const mockAxios = new MockAdapter(axios);
const middlewares: any[] = [thunk];
const mockStore: MockStoreCreator = configureMockStore(middlewares);

describe('Movies module actions', () => {
    let store: MockStore;
    const movieID: string = 'tt0371746';

    beforeEach(() => {
        store = mockStore(rootState);
    });

    describe('redux actions', () => {
        it('Should initialize movie entity', () => {
            const expected: Types.InitMovie = { type: Types.INIT_MOVIE, payload: movieID };
    
            store.dispatch(Actions.initMovie(movieID));
            expect(store.getActions()[0]).to.be.deep.equal(expected);
        });
        it('Should set loading state as true in case movie started fatching', () => {
            const expected: Types.StartMovieFetching = { type: Types.START_MOVIE_FETCHING, payload: movieID };
    
            store.dispatch(Actions.startMovieFetching(movieID));
            expect(store.getActions()[0]).to.be.deep.equal(expected);
        });
        it('Should set loading state as false in case movie finished fatching', () => {
            const expected: Types.FinishMovieFetching = { type: Types.FINISH_MOVIE_FETCHING, payload: movieID };
    
            store.dispatch(Actions.finishMovieFetching(movieID));
            expect(store.getActions()[0]).to.be.deep.equal(expected);
        });
    });
    describe('redux-thunk actions', () => {
        afterEach(() => {
            mockAxios.reset();
        });

        it('Should get movie by movie provided movie id', async () => {
            const expected : Array<Types.MoviesActionTypes> = [
                { type: Types.INIT_MOVIE, payload: movieID},
                { type: Types.START_MOVIE_FETCHING, payload: movieID},
                { type: Types.SET_MOVIE, payload: { id: movieID, data: responseMock} },
                { type: Types.FINISH_MOVIE_FETCHING, payload: movieID },
            ];

            mockAxios.onGet(API.API_ROOT_PATH).reply(200, responseMock);
            
            let result;
            await store.dispatch<any>(Actions.getMovieByID(movieID))
                .then(() => result = store.getActions())

            expect(result).to.be.deep.equal(expected);
        });
    });
});