import mocha from 'mocha';
import { expect } from 'chai';

import * as Actions from '../actions';
import * as Reducers from '../reducers';
import * as Types from '../types';

import { responseMock } from '../../../components/MovieDashboard/mock';

describe('userPref module reducers', () => {
    const movieID: string = 'tt0371746';

    it('Should be pure', () => {
        const firstAction: Types.InitMovie = Actions.initMovie(movieID);
        const secondAction: Types.InitMovie  = Actions.initMovie(movieID);

        const expected = {
            ...Reducers.MoviesInitialState,
            movies: {
                [movieID]: {}
            },
        };

        expect(Reducers.MoviesReducer(Reducers.MoviesInitialState, firstAction)).to.be.deep.equal(expected);
        expect(Reducers.MoviesReducer(Reducers.MoviesInitialState, secondAction)).to.be.deep.equal(expected);
    });
    it('Should initialize movie in movies state', () => {
        const action: Types.InitMovie = Actions.initMovie(movieID);
        const expected = {
            ...Reducers.MoviesInitialState,
            movies: {
                [movieID]: {},
            },
        };

        expect(Reducers.MoviesReducer(Reducers.MoviesInitialState, action)).to.be.deep.equal(expected);
    });
    it('Should set movie to fetching state', () => {
        const action: Types.StartMovieFetching = Actions.startMovieFetching(movieID);
        const expected = {
            ...Reducers.MoviesInitialState,
            fetching: [movieID],
        };

        expect(Reducers.MoviesReducer(Reducers.MoviesInitialState, action)).to.be.deep.equal(expected);
    });
    it('Should remove movie from fetching state', () => {
        const action: Types.FinishMovieFetching = Actions.finishMovieFetching(movieID);
        const state = {
            ...Reducers.MoviesInitialState,
            fetching: [movieID],
        };

        expect(Reducers.MoviesReducer(state, action)).to.be.deep.equal(Reducers.MoviesInitialState);
    });
    it('Should set movie data to movies state', () => {
        const action: Types.SetMovie = Actions.setMovie(movieID, responseMock);

        const state = {
            ...Reducers.MoviesInitialState,
            movies: {
                [movieID]: {},
            }
        };

        const expected = {
            ...Reducers.MoviesInitialState,
            movies: {
                [movieID]: responseMock,
            },
        };

        expect(Reducers.MoviesReducer(state, action)).to.be.deep.equal(expected);
    });
});