import { expect } from 'chai';

import configureMockStore, { MockStoreCreator } from 'redux-mock-store';

import * as Selectors from '../selectors';

import { rootState } from '../../../store';

import { responseMock } from '../../../components/MovieDashboard/mock';

const middlewares: any[] = [];
const mockStore: MockStoreCreator = configureMockStore(middlewares);

describe('Movies module selectors', () => {
    let store: any, movieID: any = responseMock.imdbID;

    beforeEach(() => {
        store = mockStore({
            ...rootState,
            movies: {
                movies: {
                    [movieID]: responseMock,
                },
                fetching: [movieID],
            },
        });
    });

    describe('makeGetMovieByID selector creator', () => {
        it('Should return movie from store by provided movieID', () => {
            const getMovieByID = Selectors.makeGetMovieByID(movieID);

            expect(getMovieByID(store.getState())).to.be.deep.equal(responseMock);
        });
    });

    describe('makeIsMovieFetching selector creator', () => {
        it('Should return isFetching state of the movie', () => {
            const isMovieFetching = Selectors.makeIsMovieFetching(movieID);

            expect(isMovieFetching(store.getState())).to.be.true;
        });
    });
});