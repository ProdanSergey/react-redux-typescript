import { Movie } from '../../components/MovieCard/types';

export interface MoviesState {
    movies: Movies;
    fetching: Array<string | never>;
};

export const INIT_MOVIE = 'INIT_MOVIE';
export const SET_MOVIE = 'SET_MOVIE';
export const START_MOVIE_FETCHING = 'START_MOVIE_FETCHING';
export const FINISH_MOVIE_FETCHING = 'FINISH_MOVIE_FETCHING';

export interface Movies {
    [index: string]: Movie;
}

interface SetMoviePaylaod {
    id: string;
    data: object;
};

export interface InitMovie {
    type: typeof INIT_MOVIE;
    payload: string;
};

export interface SetMovie {
    type: typeof SET_MOVIE;
    payload: SetMoviePaylaod;
};

export interface StartMovieFetching {
    type: typeof START_MOVIE_FETCHING;
    payload: string;
};

export interface FinishMovieFetching {
    type: typeof FINISH_MOVIE_FETCHING;
    payload: string;
};

export type MoviesActionTypes = InitMovie | SetMovie | StartMovieFetching | FinishMovieFetching;