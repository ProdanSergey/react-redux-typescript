import {
    INIT_MOVIE,
    SET_MOVIE,
    START_MOVIE_FETCHING,
    FINISH_MOVIE_FETCHING,
    MoviesState,
    MoviesActionTypes,
} from "./types";
  
export const MoviesInitialState: MoviesState = {
    movies: {},
    fetching: [],
};

export function MoviesReducer(
    state = MoviesInitialState,
    action: MoviesActionTypes
    ): MoviesState {
    switch (action.type) {
        case INIT_MOVIE:
            return {
                ...state,
                movies: {
                    ...state.movies,
                    [action.payload]: {},
                }
            }
        case SET_MOVIE: 
            return {
                ...state,
                movies: {
                    ...state.movies,
                    [action.payload.id]: {
                        ...state.movies[action.payload.id],
                        ...action.payload.data,
                    },
                },
            }
        case START_MOVIE_FETCHING: 
            return {
                ...state,
                fetching: [
                    ...state.fetching,
                    action.payload,
                ]
            }
        case FINISH_MOVIE_FETCHING: 
            return {
                ...state,
                fetching: state.fetching.filter((id: string) => id !== action.payload),
            }
        default:
            return state;
    }
}
