import { createSelector, Selector } from 'reselect';
import get from 'lodash/get';

import { AppState } from '../../store';
import { Movies } from './types';
import { Movie } from '../../components/MovieCard/types';

const getMovies = (state: AppState): Movies => state.movies.movies;
const getFetchings = (state: AppState): Array<string> => state.movies.fetching;

export const makeIsMovieFetching = (movieID: string): Selector<AppState, boolean> => 
    createSelector(getFetchings, fetchings => fetchings.includes(movieID));

export const makeGetMovieByID = (movieID: string): Selector<AppState, Movie> => 
    createSelector(getMovies, movies => get(movies, movieID, {}));