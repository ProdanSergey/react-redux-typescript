import axios, { AxiosResponse, AxiosError } from 'axios';
import isEmpty from 'lodash/isEmpty';

import { AnyAction, ActionCreator } from 'redux';

import { ThunkAction, ThunkDispatch } from 'redux-thunk';

import { AppState } from '../../store';

import { makeGetMovieByID } from './selectors';

import * as types from './types';
import * as API from '../../constants/api';
import { Movie } from '../../components/MovieCard/types';

export const initMovie: ActionCreator<types.InitMovie> = (movieID: string) : types.InitMovie => {
    return {
        type: types.INIT_MOVIE,
        payload: movieID,
    };
};

export const setMovie: ActionCreator<types.SetMovie> = (id: string, data: object) : types.SetMovie => {
    return {
        type: types.SET_MOVIE,
        payload: {
            id,
            data,
        },
    };
};

export const startMovieFetching: ActionCreator<types.StartMovieFetching> = (movieID: string) : types.StartMovieFetching => {
    return {
        type: types.START_MOVIE_FETCHING,
        payload: movieID,
    };
};

export const finishMovieFetching: ActionCreator<types.FinishMovieFetching> = (movieID: string) : types.FinishMovieFetching => {
    return {
        type: types.FINISH_MOVIE_FETCHING,
        payload: movieID,
    };
};

type TReturn = Promise<any> | Movie;

type ThunkResult = ThunkAction<TReturn, AppState, void, AnyAction>;

export const getMovieByID: ActionCreator<ThunkResult> = (movieID: string): ThunkResult => {
    return (dispatch: ThunkDispatch<AppState, undefined, AnyAction>, getState) => {
        const state = getState();
        const getMovieByID = makeGetMovieByID(movieID);

        const movie: Movie = getMovieByID(state);
        const isExist = !isEmpty(movie);

        if (isExist) return movie;

        dispatch(initMovie(movieID));
        dispatch(startMovieFetching(movieID));

        return axios.get(API.API_ROOT_PATH, {
            params: {
                i: movieID,
            },
            headers: {
                'X-RapidAPI-Host': API.API_HOST,
                'X-RapidAPI-Key': API.APY_AUTH_KEY,
            },
        })
            .then((response : AxiosResponse) => dispatch(setMovie(movieID, response.data)))
            .catch((error: AxiosError) => console.log(error))
            .finally(() => dispatch(finishMovieFetching(movieID)))        
    };
};

