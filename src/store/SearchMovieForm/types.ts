export interface SearchQueryState {
    result: Array<any>;
    isLoading: boolean;
};
export const SET_SEARCH_RESULT = 'SET_SEARCH_RESULT';
export const START_SEARCH = 'START_SEARCH';
export const FINISH_SEARCH = 'FINISH_SEARCH';

export interface SetSearchResult {
    type: typeof SET_SEARCH_RESULT;
    payload: Array<any>;
};

export interface StartSearch {
    type: typeof START_SEARCH;
};

export interface FinishSearch {
    type: typeof FINISH_SEARCH;
};

export type SearchQueryActionTypes = SetSearchResult | StartSearch | FinishSearch;