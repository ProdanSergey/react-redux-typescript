import {
    SET_SEARCH_RESULT,
    START_SEARCH,
    FINISH_SEARCH,
    SearchQueryState,
    SearchQueryActionTypes,
} from "./types";
  
export const SearchFormInitialState: SearchQueryState = {
    result: [],
    isLoading: false,
};

export function SearchFormReducer(
    state = SearchFormInitialState,
    action: SearchQueryActionTypes
    ): SearchQueryState {
    switch (action.type) {
        case SET_SEARCH_RESULT: 
            return {
                ...state,
                result: action.payload,
            }
        case START_SEARCH: 
            return {
                ...state,
                isLoading: true,
            }
        case FINISH_SEARCH: 
            return {
                ...state,
                isLoading: false,
            }
        default:
            return state;
    }
}
