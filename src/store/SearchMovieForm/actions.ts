import axios, { AxiosResponse, AxiosError } from 'axios';

import { AnyAction, ActionCreator } from 'redux';

import { ThunkAction, ThunkDispatch } from 'redux-thunk';

import { AppState } from '../../store';

import * as types from './types';
import * as API from '../../constants/api';

export const setSearchResult: ActionCreator<types.SetSearchResult> = (result: Array<any>) : types.SetSearchResult => {
    return {
        type: types.SET_SEARCH_RESULT,
        payload: result,
    };
};

export const startSearching: ActionCreator<types.StartSearch> = () : types.StartSearch => {
    return {
        type: types.START_SEARCH,
    };
};

export const finishSearching: ActionCreator<types.FinishSearch> = () : types.FinishSearch => {
    return {
        type: types.FINISH_SEARCH,
    };
};

type ThunkResult = ThunkAction<Promise<any>, AppState, void, AnyAction>

export const searchMovieByName: ActionCreator<ThunkResult> = (query: string) : ThunkResult => {
    return (dispatch: ThunkDispatch<AppState, undefined, AnyAction>) => {
        dispatch(startSearching());

        return axios.get(API.API_ROOT_PATH, {
            params: {
                s: query,
            },
            headers: {
                'X-RapidAPI-Host': API.API_HOST,
                'X-RapidAPI-Key': API.APY_AUTH_KEY,
            }
        })
            .then((response : AxiosResponse) => dispatch(setSearchResult(response.data.Search)))
            .catch((error: AxiosError) => console.log(error))
            .finally(() => dispatch(finishSearching()))
    };
};
