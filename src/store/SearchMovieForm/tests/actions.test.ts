import { expect } from 'chai';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import configureMockStore, { MockStore, MockStoreCreator } from 'redux-mock-store';

import thunk from 'redux-thunk';

import * as Actions from '../actions';
import * as Types from '../types';

import { rootState } from '../../../store';

import * as API from '../../../constants/api';
import { responseMock } from '../../../components/SearchForm/mock';

const mockAxios = new MockAdapter(axios);
const middlewares: any[] = [thunk];
const mockStore: MockStoreCreator = configureMockStore(middlewares);

describe('Search Form module actions', () => {
    describe('redux actions', () => {
        it('Should set loading state as true in case search is started', () => {
            const expected: Types.StartSearch = { type: Types.START_SEARCH };
            const store: MockStore<Types.SearchQueryState | {}> = mockStore(rootState);

            store.dispatch(Actions.startSearching());
    
            expect(store.getActions()).to.be.deep.equal([expected]);
        });
        it('Should set loading state as false in case search is finished', () => {
            const expected: Types.FinishSearch = { type: Types.FINISH_SEARCH };
            const store: MockStore<Types.SearchQueryState | {}> = mockStore(rootState);
            store.dispatch(Actions.finishSearching());
    
            expect(store.getActions()).to.be.deep.equal([expected]);
        });
    });
    describe('redux-thunk actions', () => {
        afterEach(() => {
            mockAxios.reset();
        });

        it('Should get movies by movie search query', async () => {
            const expected : Array<Types.SearchQueryActionTypes> = [
                { type: Types.START_SEARCH },
                { type: Types.SET_SEARCH_RESULT, payload: responseMock.Search },
                { type: Types.FINISH_SEARCH },
            ];
            const store: MockStore<Types.SearchQueryState | {}> = mockStore(rootState);

            mockAxios.onGet(API.API_ROOT_PATH).reply(200, responseMock);

            let result;
            await store.dispatch<any>(Actions.searchMovieByName('iron'))
                .then(() => result = store.getActions())

                expect(result).to.be.deep.equal(expected);
        });
    });
});