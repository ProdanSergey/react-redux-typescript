import { expect } from 'chai';

import * as Actions from '../actions';
import * as Reducers from '../reducers';
import * as Types from '../types';

import { responseMock } from '../../../components/SearchForm/mock';

describe('userPref module reducers', () => {
    const movieID: string = 'tt0371746';

    it('Should be pure', () => {
        const firstAction: Types.SetSearchResult = Actions.setSearchResult(responseMock.Search);
        const secondAction: Types.SetSearchResult  = Actions.setSearchResult(responseMock.Search);

        const expected = {
            ...Reducers.SearchFormInitialState,
            result: responseMock.Search,
        };

        expect(Reducers.SearchFormReducer(Reducers.SearchFormInitialState, firstAction)).to.be.deep.equal(expected);
        expect(Reducers.SearchFormReducer(Reducers.SearchFormInitialState, secondAction)).to.be.deep.equal(expected);
    });
    it('Should set search state to true', () => {
        const action: Types.StartSearch = Actions.startSearching(movieID);
        const expected = {
            ...Reducers.SearchFormInitialState,
            isLoading: true,
        };

        expect(Reducers.SearchFormReducer(Reducers.SearchFormInitialState, action)).to.be.deep.equal(expected);
    });
    it('Should set search state to false', () => {
        const action: Types.FinishSearch = Actions.finishSearching(movieID);
        
        const state = {
            ...Reducers.SearchFormInitialState,
            isLoading: true,
        };

        const expected = {
            ...Reducers.SearchFormInitialState,
            isLoading: false,
        };

        expect(Reducers.SearchFormReducer(state, action)).to.be.deep.equal(expected);
    });
    it('Should set search result to state', () => {
        const action: Types.SetSearchResult = Actions.setSearchResult(responseMock);
        
        const expected = {
            ...Reducers.SearchFormInitialState,
            result: responseMock,
        };

        expect(Reducers.SearchFormReducer(Reducers.SearchFormInitialState, action)).to.be.deep.equal(expected);
    });
});