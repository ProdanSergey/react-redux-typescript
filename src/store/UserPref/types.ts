export interface UserPrefState {
    favourites: Array<string>;
};

export const MARK_AS_FAVOURITE = 'MARK_AS_FAVOURITE';
export const UNMARK_AS_FAVOURITE = 'UNMARK_AS_FAVOURITE';
export const SAVE_FAVOURITES = 'SAVE_FAVOURITES';
export const LOAD_FAVOURITES = 'LOAD_FAVOURITES';

export interface MarkAsFav {
    type: typeof MARK_AS_FAVOURITE;
    payload: string;
};

export interface UnMarkAsFav {
    type: typeof UNMARK_AS_FAVOURITE;
    payload: string;
};

export interface SaveFavourites {
    type: typeof SAVE_FAVOURITES;
};

export interface LoadFavourites {
    type: typeof LOAD_FAVOURITES;
};

export type UserPrefActionTypes = MarkAsFav | UnMarkAsFav | SaveFavourites | LoadFavourites;