import { createSelector, Selector } from 'reselect';

import { AppState } from '../../store';

const getFavourites = (state: AppState): Array<string> => state.userPref.favourites;

export const makeIsMovieFavourite = (movieID: string = ''): Selector<AppState, boolean> => 
    createSelector(getFavourites, favourites => favourites.includes(movieID));