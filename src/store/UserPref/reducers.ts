import {
    MARK_AS_FAVOURITE,
    UNMARK_AS_FAVOURITE,
    UserPrefState,
    UserPrefActionTypes,
} from "./types";
  
export const UserPrefInitialState: UserPrefState = {
    favourites: [],
};

export function UserPrefReducer(
    state = UserPrefInitialState,
    action: UserPrefActionTypes
    ): UserPrefState {
    switch (action.type) {
        case MARK_AS_FAVOURITE: 
            return {
                ...state,
                favourites: [
                    ...state.favourites,
                    action.payload,
                ],
            }
        case UNMARK_AS_FAVOURITE: 
            return {
                ...state,
                favourites: state.favourites.filter(item => item !== action.payload),
            }
        default:
            return state;
    }
}
