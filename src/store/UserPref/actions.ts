import { ActionCreator } from 'redux';

import * as types from './types';

export const markMovieAsFavourite: ActionCreator<types.MarkAsFav> = (movieID: string) : types.MarkAsFav => {
    return {
        type: types.MARK_AS_FAVOURITE,
        payload: movieID,
    };
};

export const unmarkMovieAsFavourite: ActionCreator<types.UnMarkAsFav> = (movieID :string) : types.UnMarkAsFav => {
    return {
        type: types.UNMARK_AS_FAVOURITE,
        payload: movieID,
    };
};