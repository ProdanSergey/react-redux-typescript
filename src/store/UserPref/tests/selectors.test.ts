import { expect } from 'chai';

import configureMockStore, { MockStoreCreator } from 'redux-mock-store';

import * as Selectors from '../selectors';

import { rootState } from '../../../store';

import { responseMock } from '../../../components/MovieDashboard/mock';

const middlewares: any[] = [];
const mockStore: MockStoreCreator = configureMockStore(middlewares);

describe('Movies module selectors', () => {
    let store: any, movieID: any = responseMock.imdbID;

    beforeEach(() => {
        store = mockStore({
            ...rootState,
            userPref: {
                favourites: [movieID]
            }
        });
    });

    describe('makeIsMovieFavourite selector creator', () => {
        it('Should return isFavourite state of the movie', () => {
            const isMovieFavourite = Selectors.makeIsMovieFavourite(movieID);

            expect(isMovieFavourite(store.getState())).to.be.true;
        });
    });
});