import { expect } from 'chai';
import configureMockStore from 'redux-mock-store';
import * as Actions from '../actions';
import * as Types from '../types';
import { UserPrefInitialState } from '../reducers';

const middlewares: any[] = [];
const mockStore = configureMockStore(middlewares);

describe('userPref module actions', () => {
    const movieID: string = 'tt212333';

    it('Should mark movie as favourite', () => {
        const store = mockStore(UserPrefInitialState);
        const expected = {
            type: Types.MARK_AS_FAVOURITE,
            payload: movieID,
        };

        store.dispatch(Actions.markMovieAsFavourite(movieID));

        expect(store.getActions()).to.be.deep.equal([expected]);
    });
    it('Should remove movie from favourites', () => {
        const store = mockStore(UserPrefInitialState);
        const expected = {
            type: Types.UNMARK_AS_FAVOURITE,
            payload: movieID,
        };

        store.dispatch(Actions.unmarkMovieAsFavourite(movieID));

        expect(store.getActions()).to.be.deep.equal([expected]);
    });
});