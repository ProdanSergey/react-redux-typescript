import { expect } from 'chai';
import * as Actions from '../actions';
import { UserPrefInitialState, UserPrefReducer } from '../reducers';

describe('userPref module reducers', () => {
    const movieID: string = 'tt212333';

    it('Should be pure', () => {
        const firstAction = Actions.markMovieAsFavourite(movieID);
        const secondAction = Actions.markMovieAsFavourite(movieID);

        const expected = {
            ...UserPrefInitialState,
            favourites: [movieID],
        };

        expect(UserPrefReducer(UserPrefInitialState, firstAction)).to.be.deep.equal(expected);
        expect(UserPrefReducer(UserPrefInitialState, secondAction)).to.be.deep.equal(expected);
    });
    it('Should add movie to favourites', () => {
        const action = Actions.markMovieAsFavourite(movieID);
        const expected = {
            ...UserPrefInitialState,
            favourites: [movieID],
        };

        expect(UserPrefReducer(UserPrefInitialState, action)).to.be.deep.equal(expected);
    });
    it('Should remove movie from favourites', () => {
        const action = Actions.unmarkMovieAsFavourite(movieID);
        const state = {
            ...UserPrefInitialState,
            favourites: [movieID],
        };

        expect(UserPrefReducer(state, action)).to.be.deep.equal(UserPrefInitialState);
    });
});