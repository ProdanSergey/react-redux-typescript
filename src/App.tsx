import React, { Fragment } from 'react';

import { Switch, Route } from 'react-router'

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import Header from './components/Header/Header';
import SearchForm from './components/SearchForm/SearchForm';

import MovieDashboard from './components/MovieDashboard/MovieDashboard';
import FavouritesDashboard from './components/FavouritesDashboard/FavouritesDashboard';
import AboutUs from './components/AboutUs/AboutUs';
import NoMatch from './components/NoMatch/NoMatch';

import styles from './App.module.scss';

const App: React.FC = () => {
  return (
    <Fragment>
      <Header />
      <Container className={styles.container}>
        <Row>
          <Col>
            <SearchForm />
          </Col>
          <Col lg="9">
            <Switch>
              <Route exact path="/" component={AboutUs} />
              <Route path="/favourites" component={FavouritesDashboard} />
              <Route path="/:movieID" component={MovieDashboard} />
              <Route component={NoMatch}/>
            </Switch>
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
}

export default App;
