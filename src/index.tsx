import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import store, { history } from './store';

import App from './App';
import * as serviceWorker from './serviceWorker';

import 'react-bootstrap-typeahead/css/Typeahead.css';

ReactDOM.render((
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>
), document.getElementById('root'));

serviceWorker.unregister();
