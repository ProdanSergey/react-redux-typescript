import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { createBrowserHistory, History } from 'history';
import { routerMiddleware, connectRouter, RouterState } from 'connected-react-router';

import { SearchQueryState } from './store/SearchMovieForm/types';
import { MoviesState } from './store/Movies/types';
import { UserPrefState } from './store/UserPref/types';

import { SearchFormInitialState, SearchFormReducer } from './store/SearchMovieForm/reducers';
import { MoviesInitialState, MoviesReducer } from './store/Movies/reducers';
import { UserPrefInitialState, UserPrefReducer } from './store/UserPref/reducers';

export const history: History = createBrowserHistory();

export interface AppState {
  router: RouterState;
  searchMovieForm: SearchQueryState;
  movies: MoviesState;
  userPref: UserPrefState;
}

export const rootState: AppState = {
  router: history,
  searchMovieForm: SearchFormInitialState,
  movies: MoviesInitialState,
  userPref: UserPrefInitialState,
}

const rootReducer = combineReducers<AppState>({
  router: connectRouter(history),
  searchMovieForm: SearchFormReducer,
  movies: MoviesReducer,
  userPref: UserPrefReducer,
});

const middlewares = [routerMiddleware(history), thunkMiddleware];
const enhanser = applyMiddleware(...middlewares);

const store = createStore(
  rootReducer,
  rootState,
  composeWithDevTools(enhanser)
);

export default store;