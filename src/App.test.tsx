import React from 'react';
import { Route, Switch } from 'react-router';

import { configure, shallow } from 'enzyme';
import { expect } from 'chai';

import Adapter from 'enzyme-adapter-react-16';

import App from './App';

import Header from './components/Header/Header';
import SearchForm from './components/SearchForm/SearchForm';

import MovieDashboard from './components/MovieDashboard/MovieDashboard';
import FavouritesDashboard from './components/FavouritesDashboard/FavouritesDashboard';
import AboutUs from './components/AboutUs/AboutUs';
import NoMatch from './components/NoMatch/NoMatch';

configure({ adapter: new Adapter() });

describe('<App/> component', () => {
  const wrapper = shallow(<App />);

  describe('Component render', () => {
    it('Should render Header inside App component', () => {
      expect(wrapper.find(Header)).to.not.be.null;
    });
    it('Should render SearchForm inside App component', () => {
      expect(wrapper.find(SearchForm)).to.not.be.null;
    });
  });
  describe('Component routes', () => {
    const sw = wrapper.find(Switch);
    let pathMap: any;

    beforeAll(() => {
      pathMap = sw.find(Route).reduce((map: any, route) => {
        const props: any = route.props();
        map[props.path] = props.component;
        return map;
      }, {});
    });

    it('Component switch should render correct routes', () => {
      expect(pathMap['/']).to.equal(AboutUs);
      expect(pathMap['/favourites']).to.equal(FavouritesDashboard);
      expect(pathMap['/:movieID']).to.equal(MovieDashboard);
      expect(pathMap['undefined']).to.equal(NoMatch);
    });
  });
});